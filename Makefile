pets_comment:
	docker-compose run pets_api  alembic revision --autogenerate -m "New Migration" 
pets_migrate:
	docker-compose run pets_api alembic upgrade head

pets_error:
	docker-compose run pets_api alembic stamp head

pets_merge:
	docker-compose run pets_api alembic merge heads