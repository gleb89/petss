"""New Migration

Revision ID: 8667e9476f6f
Revises: b18f4d5c6d57
Create Date: 2021-11-23 16:35:20.099483

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '8667e9476f6f'
down_revision = 'b18f4d5c6d57'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('profile', sa.Column('email', sa.String(length=1000), nullable=False))
    op.add_column('profile', sa.Column('password', sa.String(length=1000), nullable=True))
    op.drop_column('profile', 'fairbase_id')
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('profile', sa.Column('fairbase_id', sa.VARCHAR(length=1000), autoincrement=False, nullable=False))
    op.drop_column('profile', 'password')
    op.drop_column('profile', 'email')
    # ### end Alembic commands ###
