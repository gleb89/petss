"""New Migration

Revision ID: 92d012f4fc96
Revises: 4158c3a56734
Create Date: 2021-11-23 10:35:32.398550

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '92d012f4fc96'
down_revision = '4158c3a56734'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('profile', sa.Column('email', sa.String(length=1000), nullable=False))
    op.drop_column('profile', 'fairbase_id')
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('profile', sa.Column('fairbase_id', sa.VARCHAR(length=1000), autoincrement=False, nullable=False))
    op.drop_column('profile', 'email')
    # ### end Alembic commands ###
