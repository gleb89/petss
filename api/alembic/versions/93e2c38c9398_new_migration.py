"""New Migration

Revision ID: 93e2c38c9398
Revises: e7c55d8c31dc
Create Date: 2021-11-18 20:24:27.408368

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '93e2c38c9398'
down_revision = 'e7c55d8c31dc'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    pass
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    pass
    # ### end Alembic commands ###
