import shutil
import datetime
# host = 'https://giftcity.kz:8080'
host = 'https://giftcity.kz'

import shutil
import datetime
import os

from PIL import Image 
import PIL 

from PIL import ImageFile
ImageFile.LOAD_TRUNCATED_IMAGES = True
# host = 'https://giftcity.kz:8080'
host = 'http://localhost:8001'


async def image_add(image):
    data = str(datetime.datetime.now()).replace(" ", "")
    image = Image.open(image.file)
    resized_im = image.resize((430, 430))
    resized_im.save(f'static/images/{data}.webp', format = "WebP", quality=50,optimize=True)
    origin = f'{host}/api/v1/pets/static/images/{data}.webp'
    return origin


async def image_delete(image_name):

    name_image = image_name.split('images/')[-1]
    path_dir = 'static/images'
    os.remove(path_dir + '/' + name_image)
    return True