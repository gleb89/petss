from fastapi import FastAPI ,Request
from fastapi.staticfiles import StaticFiles
from fastapi.middleware.cors import CORSMiddleware
from fastapi_jwt_auth import AuthJWT
from fastapi_jwt_auth.exceptions import AuthJWTException
from fastapi.responses import JSONResponse

from config.database import metadata, engine, database
from routers import photo, profile, comments



tags_metadata = [{
    "name": "Pets",
    "description": "Api сервиса  pets",
}]


app = FastAPI(
    openapi_url="/api/v1/pets/openapi.json",
    docs_url="/api/v1/pets/docs",
)
@app.exception_handler(AuthJWTException)
def authjwt_exception_handler(request: Request, exc: AuthJWTException):
    return JSONResponse(
        status_code=exc.status_code,
        content={"detail": exc.message}
    )
# app.add_middleware(HTTPSRedirectMiddleware)


# cors настройки!
app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)




metadata.create_all(engine)

app.state.database = database


@app.on_event("startup")
async def startup() -> None:
    database_ = app.state.database
    if not database_.is_connected:
        await database_.connect()


@app.on_event("shutdown")
async def shutdown() -> None:
    database_ = app.state.database
    if database_.is_connected:
        await database_.disconnect()

app.mount("/api/v1/pets/static", StaticFiles(directory="static"), name="static")


app.include_router(
    profile.app
)

app.include_router(
    photo.app
)

app.include_router(
    comments.app
)



