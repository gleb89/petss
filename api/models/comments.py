import datetime
from typing import Optional, List,Union,Dict
import ormar

from pydantic import BaseModel, validator

from config.database import metadata, database
from .profile import Profile




class Comments(ormar.Model):

    """ 
    """

    class Meta:
        tablename = "comments"
        metadata = metadata
        database = database

    id: int = ormar.Integer(primary_key=True)
    text: str = ormar.String(max_length=10000)
    profile:Optional[Union[Profile, Dict]] = ormar.ForeignKey(Profile, related_name="profile_comments")
    timestamp_create: datetime.datetime = ormar.DateTime(
                default=datetime.datetime.now()
                                                )  