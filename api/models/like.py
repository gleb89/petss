import datetime
from typing import Optional, List,Union,Dict
import ormar

from pydantic import BaseModel, validator

from config.database import metadata, database
from .profile import Profile



class UserLike(ormar.Model):

    """ 
    """

    class Meta:
        tablename = "user_like"
        metadata = metadata
        database = database

    id: int = ormar.Integer(primary_key=True)
