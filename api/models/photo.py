import datetime
from typing import Optional, List,Union,Dict
import ormar

from pydantic import BaseModel, validator

from config.database import metadata, database
from .profile import Profile
from .comments import Comments
from .like import UserLike


class Photo(ormar.Model):

    """ 
    """

    class Meta:
        tablename = "photo"
        metadata = metadata
        database = database

    id: int = ormar.Integer(primary_key=True)
    origin_link: str = ormar.String(max_length=1000)
    dubl_link: str = ormar.String(max_length=1000)
    name: str = ormar.String(max_length=1000,nullable=True,default='')
    small_body: str = ormar.String(max_length=1000,nullable=True,default='')
    comments: Optional[List[Comments]] = ormar.ManyToMany(
        Comments,nullable=True,related_name="self_photo"
    )

    like_user: Optional[Union[List[Profile], Dict]] = ormar.ManyToMany(
        Profile, related_name="like_users", through=UserLike
    )
    like_count: int = ormar.Integer(default=0,nullable=True)
    profile: Optional[Union[Profile, Dict]] = ormar.ForeignKey(Profile, related_name="profile_photo")
    timestamp_create: datetime.datetime = ormar.DateTime(
                default=datetime.datetime.now()
                                                )  