from fastapi import APIRouter, File, UploadFile, Depends, Form
from pydantic import BaseModel

from models.photo import Photo
from models.profile import Profile
from models.comments import Comments


app = APIRouter(
    prefix="/api/v1/pets/comments",
    tags=["Comments"],
)

class TextComments(BaseModel):
    text:str

@app.get('/')
async def get_all():
    return await Comments.objects.all()

@app.post('/{photo_id}/{profile_id}')
async def create_comment(photo_id:int, profile_id: int, new_comment:TextComments):
    profile = await Profile.objects.get(id = profile_id)
    comment = await Comments(**new_comment.dict()).save()
    await comment.update(profile = profile)
    photo = await Photo.objects.get(id = photo_id)
    await photo.comments.add(comment)
    return photo