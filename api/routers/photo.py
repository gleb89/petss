from fastapi import APIRouter, File, UploadFile, Depends, Form

from models.photo import Photo
from models.profile import Profile
from logics.photo import image_add,image_delete
from . import profile


app = APIRouter(
    prefix="/api/v1/pets/photo",
    tags=["Photo"],
)

@app.get('/')
async def get_all():
    return await Photo.objects.all()

@app.get('/{photo_id}')
async def get_one(photo_id:int):
    return await Photo.objects.select_related("comments").get(id = photo_id)       

@app.post('/{profile_id}')
async def create( 
    profile_id :int,
    name: str = Form(None),
    small_body : str = Form(None),
    image: UploadFile = File(...)
    ):
    origin_link, dubl_link = await image_add(image)
    photo =  await Photo(
        name = name,
        small_body = small_body,
        origin_link = origin_link,
        dubl_link = dubl_link
    ).save()
    profile_pets = await profile.get_one(profile_id)
    await profile_pets.profile_photo.add(photo)
    return photo

@app.post('/{photo_id}/{profile_id}')
async def add_like_photo(photo_id:int, profile_id:int):
    profile = await Profile.objects.get(id = profile_id)
    photo = await Photo.objects.select_related("like_user").get(id = photo_id)
    if profile in photo.like_user:
        photo.like_count -= 1
        await photo.like_user.remove(profile)
    else:
        photo.like_count += 1
        await photo.like_user.add(profile)
    
    await photo.update()
    return photo

@app.delete('/{photo_id}')
async def create( 
    photo_id :int
    ):
   photo = await Photo.objects.get(id=photo_id)
   await image_delete(photo.origin_link)
   await image_delete(photo.dubl_link)
   await photo.delete()