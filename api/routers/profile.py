from fastapi import APIRouter,Form,File,UploadFile,Depends, Request, HTTPException
from fastapi.responses import JSONResponse
from fastapi.encoders import jsonable_encoder
from fastapi_jwt_auth import AuthJWT
from fastapi_jwt_auth.exceptions import AuthJWTException
from pydantic.main import BaseModel

from models.profile import Profile
from logics.profile import image_add, image_delete
from logics.hashpassword import *
from logics.auth import admin_token, jwt_auth



app = APIRouter(
    prefix="/api/v1/pets/profile",
    tags=["Профиль"],
)
class User(BaseModel):
    email:str
    password:str

class Settings(BaseModel):
    authjwt_secret_key: str = "secret"

@AuthJWT.load_config
def get_config():
    return Settings()



RequestProfile = Profile.get_pydantic(exclude={
    "profile_comments",
    "profile_photo",
    "timestamp_create",
    "id",
    "avatar",
    "small_body",
    "like_users"
    })

@app.get('/')
async def get_all():
    return await Profile.objects.prefetch_related(
        ['profile_photo']
        ).all()

@app.get('/{profile_id}')
async def get_one(profile_id:int):
    return await Profile.objects.prefetch_related(
        ['profile_photo']
        ).get(id = profile_id)


@app.get('/data/auth')
async def get_one2(profile: AuthJWT = Depends()):
    current_user = profile.get_jwt_subject()
    profil =  await Profile.objects.prefetch_related(
        ['profile_photo']
        ).get(password = current_user)
    context = {
            'user':profil ,
            }
    json_compatible_item_data = jsonable_encoder(context )
    return JSONResponse(
    status_code=200,
    content=json_compatible_item_data
    )
    


@app.post('/login')
async def get_one3(data:User, Authorize: AuthJWT = Depends()):
    profile =  await Profile.objects.get(email = data.email)
    if verify_password(profile.password, data.password):
        access_token = Authorize.create_access_token(subject=profile.password)
        refresh_token = Authorize.create_refresh_token(subject=profile.password)
        context = {
            "token": access_token,
            "refresh_token":refresh_token
            }
        json_compatible_item_data = jsonable_encoder(context )
        return JSONResponse(
        status_code=200,
        content=json_compatible_item_data
        )
    

@app.post('/refresh/token')
def refresh(Authorize: AuthJWT = Depends()):
    """
    The jwt_refresh_token_required() function insures a valid refresh
    token is present in the request before running any code below that function.
    we can use the get_jwt_subject() function to get the subject of the refresh
    token, and use the create_access_token() function again to make a new access token
    """
    Authorize.jwt_refresh_token_required()

    current_user = Authorize.get_jwt_subject()
    new_access_token = Authorize.create_access_token(subject=current_user)
    context = {
        "token": new_access_token 
        }
    json_compatible_item_data = jsonable_encoder(context )
    return JSONResponse(
    status_code=200,
    content=json_compatible_item_data
    )

        
@app.post('/')
async def create(profile:RequestProfile):
    return await Profile(**profile.dict()).save()

@app.post('/update/{profile_id}')
async def update(
    profile_id: int,
    avatar:UploadFile = File(None),
    small_body:str = Form(None),
    name:str = Form(None),
    ):
    profile = await get_one(profile_id)
    if avatar:
        if profile.avatar:
            await image_delete(profile.avatar)
        link = await image_add(avatar)
        await profile.update(avatar = link)
    if small_body:
        await profile.update(small_body = small_body)
    if name:
        await profile.update(name = name)
    return profile