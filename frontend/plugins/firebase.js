import firebase from 'firebase'

let app = null;
const firebaseConfig = {
    apiKey: "AIzaSyDsjTOysLKFJSP7B_eEoFRw8kTB_hSHWaA",
    authDomain: "petstap-3b328.firebaseapp.com",
    projectId: "petstap-3b328",
    storageBucket: "petstap-3b328.appspot.com",
    messagingSenderId: "77489367491",
    appId: "1:77489367491:web:877a890b5ff3515a513576",
    measurementId: "G-HZZJ9CHK8K"
  };

app = firebase.apps.length
    ? firebase.app()
    : firebase.initializeApp(firebaseConfig)

export const db = app.database()